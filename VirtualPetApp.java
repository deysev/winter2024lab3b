import java.util.Scanner;

public class VirtualPetApp {
	public static void main (String [] args) {
		Dogs[] pack = new Dogs[3];
		for (int i = 0; i < pack.length; i++) {
			java.util.Scanner reader = new java.util.Scanner(System.in);
			//input dog speed
			System.out.println("How fast is the dog? Fast or Slow?");
			String speed = reader.nextLine();
			//input dog diet
			System.out.println("What diet does the dog have? Strict or Anything?");
			String diet = reader.nextLine();
			//input dog age
			System.out.println("How old is the dog?");
			int age = Integer.parseInt(reader.nextLine());
			pack[i] = new Dogs(speed, diet, age);
		}	
		Dogs lastAnimal = pack[pack.length - 1];
		System.out.println("Speed: " + lastAnimal.speed);
		System.out.println("Diet: " + lastAnimal.diet);
		System.out.println("Age: " + lastAnimal.age);
		
		pack[0].EatFoods();
		pack[0].RunFast();
	}
}