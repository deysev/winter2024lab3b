public class Dogs {
	
	public String speed;
	public String diet;
	public int age;

	public Dogs(String speed, String diet, int age) {
		this.speed = speed;
		this.diet = diet;
		this.age = age;
	}

	public void EatFoods() {
		if ( this.age < 9 ) {
			this.diet = "Strict";
		} else {
			this.diet = "Anything";
		}
		System.out.println(this.diet);
	}
	
	public void RunFast() {
		if (this.age < 7) {
			this.speed = "Slow";
		} else {
			this.speed = "Fast";
		}
		System.out.println(this.speed);
	}
}